{
    // Initialize blood particle
    global.ps_water_landing_part = part_type_create();
    var water_landing_part = global.ps_water_landing_part;
    //part_system_depth(ps_water_landing, 10);
    
    // Settings
    part_type_shape(water_landing_part, pt_shape_square);
    part_type_size(water_landing_part, (0.08 * argument0), (0.09 * argument0), 0, 0);
    part_type_color2(water_landing_part, make_color_rgb(44,141,215), make_color_rgb(112,181,240));
    part_type_speed(water_landing_part, 1+(2*argument2), 2+(2*argument2), 0, 0);
    
    part_type_direction(water_landing_part, argument1-20, argument1+20, 0, 0);
    //part_type_direction(water_landing_part, 70, 100, 0, 0);
    part_type_gravity(water_landing_part, 0.2, 270);
    part_type_life(water_landing_part, 15*(argument0), 20*(argument0));
}
