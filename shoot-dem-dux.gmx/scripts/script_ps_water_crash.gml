{
    // Initialize blood particle
    global.ps_water_crash_part = part_type_create();
    var water_crash_part = global.ps_water_crash_part;
    
    // Settings
    part_type_shape(water_crash_part, pt_shape_disk);
    part_type_size(water_crash_part, (0.08 * argument0), (0.10 * argument0), -0.001, 0);
    part_type_color2(water_crash_part, make_color_rgb(202,232,255), make_color_rgb(89,181,255));
    part_type_speed(water_crash_part, (argument0+argument1)*0.3, (argument0+argument1)*0.6, 0, 0);
    part_type_direction(water_crash_part, 70-(argument2*15), 100-(argument2*15), 0, 0);
    part_type_gravity(water_crash_part, (0.2*argument0), 270);
    part_type_life(water_crash_part, 2*(argument0+argument1), 5*(argument0+argument1));
}
