{
    // Initialize blood particle
    global.ps_feathers_part = part_type_create();
    var feather_part = global.ps_feathers_part;
    
    // Settings
    part_type_sprite(feather_part, spr_feather, false, false, false);
    part_type_size(feather_part, (0.9 * argument0), (1.2 * argument0), 0, 0);
    //part_type_color2(blood_part, c_red, c_maroon);
    part_type_speed(feather_part, 1, 1.5, 0, 0);
    part_type_direction(feather_part, 0, 360, 0, 0);
    part_type_gravity(feather_part, 0.05, 270);
    part_type_orientation(feather_part, 0, 260, 0, 0, 0);
    part_type_life(feather_part, 25, 35);
}
