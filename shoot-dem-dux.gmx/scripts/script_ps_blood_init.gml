{
    // Initialize blood particle
    global.ps_blood_part = part_type_create();
    var blood_part = global.ps_blood_part;
    
    // Settings
    part_type_shape(blood_part, pt_shape_disk);
    part_type_size(blood_part, (0.05 * argument0), (0.10 * argument0), 0, 0);
    part_type_color2(blood_part, c_red, c_maroon);
    part_type_speed(blood_part, 1, 3, 0, 0);
    part_type_direction(blood_part, 260, 280, 0, 0);
    part_type_gravity(blood_part, 0.3, 270);
    part_type_life(blood_part, 5, 10);
}
